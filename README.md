# Demo of pulling secrets from Vault and using GitLab JWT for "Password Zero"

## Summary
The purpose of this project to to test the ability to pull secrets from [Vault](https://www.vaultproject.io) and inject them into the pipeline. Pulling secrets from Vault is not a new capability as previously you could use an [AppRole](https://www.vaultproject.io/docs/auth/approle), which is just a pre-shared key used to authenticate with Vault. However this poses a problem. How do you protect the AppRole secret. If you get the pre-shared key, you have access to everything in Vault (based on the AppRole policy). GitLab in version 12.10 has added support for a [JSON Web Token](https://www.vaultproject.io/docs/auth/jwt) to be injected into every CI Job via a [Predefined Environment Variable](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) `CI_JOB_JWT`. This solves the "Password 0" problem. JWT is a single use token that can be used to authenticate to Vault Server. No longer will you need to store pre-shared secrets in GitLab. 


GitLab has an [example in the documentation](https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/) however it uses the [vault client](https://www.vaultproject.io/downloads) to authenticate and pull secrets. Additionally, it requires manual loading of secrets into environment variables. To load those secrets into environment variables, [Hashicorp recommends](https://learn.hashicorp.com/vault/identity-access-management/vault-agent-aws#additional-discussion) the use of [envconsul](https://github.com/hashicorp/envconsul).

>  Furthermore, since Vault Agent Auto-Auth only addresses the challenges of obtaining and managing authentication tokens (secret zero), you might want to use helper tools such as Consul Template and Envconsul to obtain secrets stored in Vault (e.g. DB credentials, PKI certificates, AWS access keys, etc.).


## How This Project Works
There are three CI Jobs that perform the same task in three different ways. All use `$CI_JOB_JWT` as the authentication method to the Vault Server.

1. Vault Client
1. Envconsul
1. [GitLab Secrets](https://docs.gitlab.com/ee/ci/yaml/#secrets) (Requires 13.4 & [Premium/Silver](https://about.gitlab.com/pricing/premium/) Subscription)


### Vault Client
Vault Client contains all the bits we need to authenticate and pull secrets give `$VAULT_TOKEN` and `$VAULT_ADDR` are defined. 
```sh
export VAULT_ADDR="https://vault.example.com:8200"
export VAULT_TOKEN="$(vault write -field=token auth/jwt/login jwt=$CI_JOB_JWT)"
vault kv get -field=DEMO_NAME secret/demo
```

### Envconsul
Assuming that Vault Client cannot be pre-installed in the CI Job, this example uses `curl` to fetch the `$VAULT_TOKEN`. [Envconsul](https://github.com/hashicorp/envconsul) requires a configuration file passed via the command line. This defines what secrets should be fetched when run. Envconsul is designed to pull secrets from Vault, store them as Environment Variables and keep them "fresh". It will continuously re-authenticate with Vault server and load the secret into the Environment Variable. This is great for short lived secrets.

```sh
export VAULT_LOGIN_OUTPUT=$(curl -s --request POST --data '{\"jwt\": \"'$CI_JOB_JWT'\"}' $VAULT_ADDR/v1/auth/jwt/login)
export VAULT_TOKEN=$(echo $VAULT_LOGIN_OUTPUT | jq -r '.auth.client_token')
envconsul -config .vault/envconsul.hcl someprocessthatneedssecrets.sh
```

### GitLab Secrets
New in 13.4 the [GitLab CI Syntax](https://docs.gitlab.com/ee/ci/yaml/#secrets) defines a new stanza for jobs. Given `$VAULT_SERVER_URL` is defined, and [any other authentication parameters](https://docs.gitlab.com/ee/ci/secrets/index.html#configure-your-vault-server) that are not default. GitLab CI Runner will pull each secret and write them to a temporary file. The file path will be stored as the given environment variable.

```yaml
# .gitlab-ci.yml
job1:
  variables:
    VAULT_SERVER_URL: "https://vault.example.com"
  secrets:
    DEMO_NAME:
      vault: infrastructure/demo/DEMO_NAME@secret
    DEMO_DOMAIN:
      vault: infrastructure/demo/DEMO_DOMAIN@secret
  script:
    - export DEMO_NAME=$(cat $DEMO_NAME)
    - export DEMO_DOMAIN=$(cat $DEMO_DOMAIN)
    - echo "The Name is $DEMO_NAME and the Domain is $DEMO_DOMAIN"
```




### envconsul [![Build Status](https://circleci.com/gh/hashicorp/envconsul.svg?style=svg)](https://circleci.com/gh/hashicorp/envconsul)

[Envconsul](https://github.com/hashicorp/envconsul) provides a convenient way to launch a subprocess with environment variables populated from HashiCorp [Consul](https://www.consul.io/) and [Vault](https://www.vaultproject.io). The tool is inspired by [envdir](https://pypi.org/project/envdir/) and [envchain](https://github.com/sorah/envchain), but works on many major operating systems with no runtime requirements. It is also available via a Docker container for scheduled environments.
